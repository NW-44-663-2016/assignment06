using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using DataRavilla.Models;

namespace DataRavilla.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20160222002450_InitialMiol")]
    partial class InitialMiol
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataRavilla.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<int?>("ScheduleScheduleID");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("DataRavilla.Models.Schedule", b =>
                {
                    b.Property<int>("ScheduleID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("EndTime");

                    b.Property<string>("ScheduleDay");

                    b.Property<string>("ScheduleType");

                    b.Property<int>("StartTime");

                    b.HasKey("ScheduleID");
                });

            modelBuilder.Entity("DataRavilla.Models.Location", b =>
                {
                    b.HasOne("DataRavilla.Models.Schedule")
                        .WithMany()
                        .HasForeignKey("ScheduleScheduleID");
                });
        }
    }
}
