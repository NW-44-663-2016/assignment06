using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace DataRavilla.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "StartTime",
                table: "Schedule",
                nullable: false);
            migrationBuilder.AlterColumn<string>(
                name: "ScheduleType",
                table: "Schedule",
                nullable: true);
            migrationBuilder.AlterColumn<string>(
                name: "ScheduleDay",
                table: "Schedule",
                nullable: true);
            migrationBuilder.AlterColumn<int>(
                name: "EndTime",
                table: "Schedule",
                nullable: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "StartTime",
                table: "Schedule",
                nullable: false);
            migrationBuilder.AlterColumn<string>(
                name: "ScheduleType",
                table: "Schedule",
                nullable: false);
            migrationBuilder.AlterColumn<string>(
                name: "ScheduleDay",
                table: "Schedule",
                nullable: false);
            migrationBuilder.AlterColumn<double>(
                name: "EndTime",
                table: "Schedule",
                nullable: false);
        }
    }
}
