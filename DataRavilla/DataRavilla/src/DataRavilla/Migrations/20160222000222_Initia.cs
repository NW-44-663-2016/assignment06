using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace DataRavilla.Migrations
{
    public partial class Initia : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ScheduleScheduleID",
                table: "Location",
                nullable: true);
            migrationBuilder.AddForeignKey(
                name: "FK_Location_Schedule_ScheduleScheduleID",
                table: "Location",
                column: "ScheduleScheduleID",
                principalTable: "Schedule",
                principalColumn: "ScheduleID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Location_Schedule_ScheduleScheduleID", table: "Location");
            migrationBuilder.DropColumn(name: "ScheduleScheduleID", table: "Location");
        }
    }
}
