﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataRavilla.Models;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace DataRavilla.Models
{
    public static class AppSeedData
    { 

        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {
            string relPath = appPath + "//Models//SedData//";
            var context = serviceProvider.GetService<AppDbContext>();
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            context.Schedules.RemoveRange(context.Schedules);
            context.Locations.RemoveRange(context.Locations);
            context.SaveChanges();

            SeedLocationsFromCsv(relPath, context);
            SeedScheduleFromCsv(relPath, context);
        }


          /*  var l1 = context.Locations.Add(new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" });
            var l2 = context.Locations.Add(new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" });
            var l3 = context.Locations.Add(new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" });
            var l4 = context.Locations.Add(new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" });
            var l5 = context.Locations.Add(new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" });
            var l6 = context.Locations.Add(new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" });
            var l7 = context.Locations.Add(new Location() { Latitude = 36.8019385, Longitude = 44.6347775, Place = "Kent", Country = "USA" });

            var k1 = context.Schedules.Add(new Schedule() { ScheduleDay = "Monday", ScheduleType = "Retail", StartTime = 0750, EndTime = 1000 });
            var k2 = context.Schedules.Add(new Schedule() { ScheduleDay = "Tuesday", ScheduleType = "Retail", StartTime = 1750, EndTime = 1000 });
            var k3 = context.Schedules.Add(new Schedule() { ScheduleDay = "Wednesday", ScheduleType = "Retail", StartTime = 0750, EndTime = 2000 });
            var k4 = context.Schedules.Add(new Schedule() { ScheduleDay = "Thursday", ScheduleType = "Retail", StartTime = 1350, EndTime = 1800 });
            var k5 = context.Schedules.Add(new Schedule() { ScheduleDay = "Friday", ScheduleType = "Non-Retail", StartTime = 1750, EndTime = 1300 });
            var k6 = context.Schedules.Add(new Schedule() { ScheduleDay = "Saturday", ScheduleType = "Non-Retail", StartTime = 1600, EndTime = 1900 });
            var k7 = context.Schedules.Add(new Schedule() { ScheduleDay = "Sunday", ScheduleType = "NonRetail", StartTime = 1730, EndTime = 2000 }
            );
            context.SaveChanges();
        }
        */


        private static void SeedScheduleFromCsv(String relPath, AppDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            //context.SaveChanges();
        }

        private static void SeedLocationsFromCsv(String relPath, AppDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            //context.SaveChanges();
        }
    }
}




   