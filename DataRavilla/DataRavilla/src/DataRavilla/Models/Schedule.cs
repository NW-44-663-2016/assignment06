﻿using DataRavilla.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DataRavilla.Models
{
    public class Schedule
    {
        //This column requires value to be inserted in the sample data file
        [Key]
        [Required]
        [ScaffoldColumn(false)]
        public int ScheduleID { get; set; }

        [RegularExpression(@"(?i)^[a-z]+")]
        public string ScheduleDay { get; set; }

        [RegularExpression(@"Retail|Non-Retail", ErrorMessage = "You must enter either Retail type or Non- Retail type")]
        public string ScheduleType { get; set; }

        [Required(ErrorMessage = "Please enter a start time")]
        [Range(0000, 2359)]
        public int StartTime { get; set; }
        [Required(ErrorMessage = "Please enter a end time")]
        [Range(0000, 2359)]
        public int EndTime { get; set; }
        public List<Location> filmSets { get; set; }

        public static List<Schedule> ReadAllFromCSV(string filepath)
        {
            List<Schedule> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Schedule.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static Schedule OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Schedule item = new Schedule();

            int i = 0;
            item.ScheduleID = Convert.ToInt32(values[i++]);
            item.ScheduleDay = Convert.ToString(values[i++]);
            item.ScheduleType = Convert.ToString(values[i++]);
            item.StartTime = Convert.ToInt32(values[i++]);
            item.EndTime = Convert.ToInt32(values[i++]);


            return item;
        }
    }
}
  